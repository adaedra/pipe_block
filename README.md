# PipeBlock

PipeBlock allows to do easy method piping through a block for easier
method chaining, including complex chaining with conditions.

On classic ruby, you'd need to reaffect a method when chaining conditionally:

```ruby
scope = Post
if params[:drafts]
  scope = scope.drafts
end
```

With pipe:

```ruby
require 'pipe_block'
using PipeBlock

scope = Post.pipe do |scope|
  # No need to re-affect, it is done automatically internally
  scope.drafts if params[:drafts]
end
```

This example does not shows much by its small size; but on more complex method calls (usual, for example, in Rails application with
complex scopes using params to refine searches) it allows to compact things
a little and stop re-affecting the variable yourself at each line.

If you need to pass the current scope to a method and want to stop the automatic re-affecting, you can use the `#unpipe` method:

```ruby
array.pipe do |scope|
  scope.map &:to_i
  # We want to pass the count to a method, but don't want to modify the scope:
  run_something scope.unpipe.count
  # Here, we still have an array
  scope.reject &:odd?
end
```
