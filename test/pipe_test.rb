require 'test_helper'
require 'pipe_block'
using PipeBlock

describe PipeBlock do
  describe 'pipe' do
    it 'basically works' do
      res = [1, 2, 3].pipe do |scope|
        scope.map { |o| [o, o * 3] }
        scope.to_h
      end

      assert_equal({1 => 3, 2 => 6, 3 => 9}, res)
    end

    it 'doesn\'t change call scope' do
      ext = 2
      res = [2, 4].pipe do |scope|
        scope.map { |n| n * ext }
      end

      assert_equal [4, 8], res
    end

    it 'works with chaining on the same line' do
      res = [2, 2].pipe do |scope|
        scope.map(&:to_s).map { |n| n * 3 }.join '|'
      end

      assert_equal '222|222', res
    end
  end

  describe 'unpipe' do
    it 'gives the current object raw' do
      res = [1].pipe do |scope|
        scope.map(&:to_s)
        assert_equal 1, scope.unpipe.count
        scope.join
      end

      assert_equal '1', res
    end
  end
end
