require 'delegate'

# PipeBlock is an implementation of a method allowing to pipe methods on
# an object in an easier way, including conditional calls.
module PipeBlock
  # The `Proxy` class is used internally to provide the block context. At
  # each method call, it will change the underlying object to the last call
  # result.
  class Proxy < SimpleDelegator
    # Catches all calls to the underlying object to make the switch after
    # execution.
    def method_missing(*)
      __setobj__ super
      self
    end

    # Gets the current object raw, to be used in other methods who can call
    # methods on it without changing it.
    alias_method :unpipe, :__getobj__
  end

  refine Object do
    # Pipe methods on a given object in the given block. Note that all
    # methods called on the object will actually change the callee, except
    # if you call `#unpipe`.
    def pipe
      Proxy.new(self).tap { |o| yield o }.__getobj__
    end
  end
end
