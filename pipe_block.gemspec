Gem::Specification.new do |spec|
  spec.name     = 'pipe_block'
  spec.version  = '1.0.0'
  spec.license  = 'MIT'
  spec.summary  = 'A simple method to pipe methods with a block'
  spec.authors  = ['Thibault `Adædra` Hamel']
  spec.files    = %w[lib/pipe_block.rb]
  spec.homepage = 'https://gitlab.com/adaedra/pipe_block'

  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'minitest-reporters'
end
